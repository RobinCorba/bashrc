# Aliases
alias ll='ls -lah'

# Docker commands
alias dup='docker compose up'
alias dab='docker compose up'
alias dbuild='docker compose up --build'
alias dopen='docker exec -it $(pbpaste) bash' # Copy name/ID of container in clip board first
alias ddown='docker compose down'

# Docker container aliases
alias dl='docker ps -a' # List all containers
alias dlopen='docker ps --format "table {{.ID}}\t{{.Status}}\t{{.Ports}}\t{{.Names}}"' # List only running containers

# Git aliases
alias didder='git push -f'
alias oopsy='git commit --amend'

alias pubkey='cat ~/.ssh/id_rsa.pub'

force_color_prompt=yes
# PS1='\[\033[1;36m\]\u\[\033[1;31m\]@\[\033[1;32m\]\h:\[\033[1;35m\]\w\[\033[1;31m\]\$\[\033[0m\] '
